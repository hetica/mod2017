from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'', include('main.urls')),
    # url(r'^$', 'mod2017_django.views.home', name='home'),
    # url(r'^mod2017_django/', include('mod2017_django.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'.*', 'main.views.accueil'),
)
