
# A FAIRE

Liste des choses à faire pour ce site qui ne verra pleut-être jamais le jour. Et il y en a des choses à faire...

## Styles

Beaucoup de choses à revoir, notamment :
* Le logo est à changer par celui réalisé par Audrey
* Les liens sont moches (récupérer les styles que j'avais fait pour mon activité libérale)
* le texte d'en tête à décaler un peu vers la droite

## Accueil

* Ajouter le texte demandé par ASFL ("Faculté des Sciences de l'Université de Montpellier")
* Le texte n'est pas très clair, il faut bien préciser
	* Lundi 06 février : conférences --> avec un lien vers le programme des confs
	* Mardi 07 février : workshops --> avec un lien vers les workshops
		* Workshops découverte
		* Workshop avancé

## Inscriptions

* scripts JavaScript pour
	* on ne peut pas cocher à la fois workshop découverte et avancé
	* au moins une des quatres cases doit être cochée (en bonus)
* ??? l'inscription doit-elle engendrer un mail pour l'équipe des MOD 2017 ???
* aligner à droite les tags
* revoir les marges verticales

## Organisateurs
* Les organisateurs pourraient être dans un tableau à deux colonnes (faculatif))

## Localisation
* Change le plan d'accès

## Aside
* Un petit mot de remerciement pour les partenaires
* compléter les partenaires
* remplacer le "mailto" par un formulaire d'envoi de mail (facultatif) avec captcha (facultatif)

## Admin
* finir de saisir tout le monde
* Pouvoir télécharger les inscriptions en format tabulaire, voir :
	* **mots clés : "django admin export"
	* http://django-import-export.readthedocs.io/en/latest/installation.html
	* https://github.com/edcrewe/django-csvimport/blob/master/README.rst
	* https://pypi.python.org/pypi/django-admin-export
	* https://pypi.python.org/pypi/django-export
	* http://davidmburke.com/2011/05/17/django-admin-better-export-to-xls/
	* http://davidmburke.com/2011/05/17/django-admin-better-export-to-xls/
