from django.contrib import admin
from main.models import Inscrit

class InscritAdmin(admin.ModelAdmin):
	list_display = ('nom', 'prenom', 'mail', 'institut', 'ville')
	# ordering = ('nom', 'prenom', 'etablissement', 'ville')

admin.site.register(Inscrit, InscritAdmin)
