from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('main.views',
    url(r'^$', 'accueil'),
    url(r'^accueil', 'accueil'),
    url(r'^programme', 'programme'),
    url(r'^inscriptions', 'inscriptions', name='inscriptions'),
    url(r'^photos', 'photos'),
    url(r'^organisateurs', 'organisateurs'),
    url(r'^partenaires', 'partenaires'),
    url(r'^telechargements', 'telechargements'),
    url(r'^localisation', 'localisation'),
    )
