#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def sendMail(prenom="John", nom="Doe", mail="benoit.guibert@free.fr"):

	# De qui, pour qui ?
	fromaddr = "noreply@mod2017.fr"
	toaddr = mail

	# Élaborer le message
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "Inscription aux Montpellier OMICS Days 2017"
	body = '''Bonjour {} {},\n\nNous sommes heureux de vous rencontrer bientôt aux Montpellier OMICS Days 2017.
	\n\nPour tous renseignements, vous pouvez nous contacter par e-mail à l'adresse montpellier.omicsdays@gmail.com, ou sur les réseaux sociaux :
	\n- Lien Twitter: https://twitter.com/OmicsDays\n- Lien Facebook : https://www.facebook.com/events/1347669318591238/\n\n--\nLes organisateurs'''.format(prenom, nom)
	msg.attach(MIMEText(body, _charset='utf-8'))

	# Se connecter et envoyer le message
	smtp = smtplib.SMTP()
	smtp.connect()
	smtp.sendmail(fromaddr,toaddr,msg.as_string() )
	smtp.quit()

if __name__ == "__main__":
	senMail()
