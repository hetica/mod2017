# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class Inscrit(models.Model):
    FEMME = 'F'
    HOMME = 'H'
    NR = '-'
    SEXE = (
        (HOMME, 'Homme'),
        (FEMME, 'Femme'),
        (NR, ' --- '),
    )

    BIOINFO = "01 bioinfo"
    INFORMATIQUE = "02 informatique"
    BIOLOGIE = "03 biologie"
    TOUS = "98 tous"
    AUCUN = "99 aucun"
    INTERETS = (
        (BIOINFO, "Bioinformatique"),
        (INFORMATIQUE, "Informatique"),
        (BIOLOGIE, "Biologie"),
        (TOUS, "Tous"),

    )

    MAIL = "01 email"
    INTERNET = "02 internet"
    MEDIA = "03 presse-radio"
    BOUCHEAOREILLE = "04 bouche a oreille"
    CONNAISSANCES = "98 connaissances-proches"
    AUTRE = "99 autre"
    CONNAITOMICS = (
        ( MAIL, "E-mail" ),
        ( INTERNET, "Internet" ),
        ( MEDIA, "Autre média (presse, radio)" ),
        ( BOUCHEAOREILLE, "Bouche à oreille" ),
        ( CONNAISSANCES, "Je connais un/des organisateurs ou proches" ),
        ( AUTRE, "Autre" ),
    )

    JAMAIS = 0
    UNEFOIS = 1
    DEUXFOIS = 2
    TROISFOIS = 3
    QUATREFOIS = 4
    DEJAPARTICIPE = (
        ( JAMAIS, "Jamais" ),
        ( UNEFOIS, "Une fois" ),
        ( DEUXFOIS, "Deux fois" ),
        ( TROISFOIS, "Trois fois" ),
        ( QUATREFOIS, "Depuis le début" ),
    )

    TECHNICIEN = 'TECH'
    INGENIEUR = 'INGE'
    PROFESSEUR = 'PROF'
    DOCTEUR = 'DOCT'
    ENSEIGNANT = 'ENSG'
    ENSEIGNANT_CHERCHEUR = 'E-CH'
    CHERCHEUR = "CHER"
    ETUDIANT = 'ETUD'
    AUTRE_GRADE = 'OTHE'
    SITUATION = (
        (ETUDIANT, 'Étudiant'),
        (TECHNICIEN, 'Technicien'),
        (ENSEIGNANT, 'Enseignant'),
        (ENSEIGNANT_CHERCHEUR, 'Enseignant Chercheur'),
        (CHERCHEUR, 'Chercheur'),
        (INGENIEUR, 'Ingénieur'),
        (PROFESSEUR, 'Professeur'),
        (DOCTEUR, 'Docteur'),
        (AUTRE_GRADE, 'Autre'),
    )

    nom = models.CharField(max_length=100, verbose_name="Nom *")
    prenom = models.CharField(max_length=100, verbose_name="Prénom *")
    mail = models.EmailField(max_length=254, unique=True, verbose_name="E-mail *")
    situation = models.CharField(max_length=100, choices=SITUATION, verbose_name="Situation Actuelle *")
    institut = models.CharField(max_length=200, verbose_name="Institut/établissement *")
    # age = models.PositiveSmallIntegerField(verbose_name="Age", null=True, blank=True)
    sexe = models.CharField(max_length=1, choices=SEXE, default=NR)

    ville = models.CharField(max_length=100, verbose_name="Ville *")
    pays = models.CharField(max_length=100, verbose_name="Pays *")
    situation = models.CharField(max_length=20, verbose_name="Situation actuelle", choices=SITUATION, default=INGENIEUR)
    # cp = models.IntegerField(max_length=5, verbose_name="Code postal")
    dateInscription = models.DateField(auto_now=True)

    confAM = models.BooleanField(verbose_name="aux conférences du lundi matin")
    confPM = models.BooleanField(verbose_name="aux conférences du lundi après-midi")
    workshop_L1 = models.BooleanField(verbose_name="au workshop découverte")
    workshop_L2 = models.BooleanField(verbose_name="au Workshop avancé")

    interets = models.CharField(max_length=20, verbose_name="Centres d'intérêts", choices=INTERETS, default=BIOINFO, null=True, blank=True)
    connaitOmics = models.CharField(max_length=30,  verbose_name="Comment avez-vous entendu parler de notre manifestation ?", choices=CONNAITOMICS, default=INTERNET)
    dejaParticipe = models.SmallIntegerField(choices=DEJAPARTICIPE,  verbose_name="Avez-vous déjà participé aux Montpelliers OMICS Days ?", default=JAMAIS)
    droitImage = models.BooleanField(verbose_name="J'autorise la publication de mon image.", default=True)


    def __unicode__(self):
        return u"%s" % self.mail

