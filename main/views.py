# -*- coding: utf-8 -*-

from django.shortcuts import render
from main.forms import InscriptionForm
# from django.conf import settings
from django.contrib.auth.models import User
from main.lib.sendAckInscriptionByMail import sendMail
from main.models import Inscrit

# Create your views here.
def accueil(request):
    return render(request, 'main/accueil.html')

def programme(request):
    return render(request, 'main/programme.html')
    
def photos(request):
    return render(request, 'main/photos.html')

def inscriptions(request):
    """ Formulaire d'inscription aux Montpelliers Omics Days"""
    # tester le nombre d'inscrits aux conférence et aux workshops
    no_inscrits_confAM = 0 ; max_confAM = 300
    no_inscrits_confPM = 0 ; max_confPM = 300
    no_inscrits_workshop_l1 = 0 ; max_wshopL1 = 36
    no_inscrits_workshop_l2 = 0 ; max_wshopL2 = 36
    for inscrit in Inscrit.objects.all():
        if inscrit.confAM:
            no_inscrits_confAM +=1
        if inscrit.confPM:
            no_inscrits_confPM +=1
        if inscrit.workshop_L1:
            no_inscrits_workshop_l1 +=1
        if inscrit.workshop_L2:
            no_inscrits_workshop_l2 +=1
    rest_conAM = max_confAM - no_inscrits_confAM

    # nombre de places restantes
    rest_conPM = max_confAM - no_inscrits_confAM if max_confAM - no_inscrits_confAM > 0 else 0
    rest_conPM = max_confPM - no_inscrits_confPM if max_confPM - no_inscrits_confPM > 0 else 0
    rest_wshopL1 = max_wshopL1 - no_inscrits_workshop_l1 if max_wshopL1 - no_inscrits_workshop_l1 > 0 else 0
    rest_wshopL2 = max_wshopL2 - no_inscrits_workshop_l2 if max_wshopL2 - no_inscrits_workshop_l2 > 0 else 0

    if request.method == 'POST':
        # appel du formulaire Inscriptions créé dans forms.py
        form = InscriptionForm(request.POST)

        if form.is_valid():
            form.save()
            sendMail(prenom=form.cleaned_data['prenom'].encode('utf-8'), nom=form.cleaned_data['nom'].encode('utf-8'), mail=form.cleaned_data['mail'].encode('utf-8') )
            envoi = True
    else:

        form = InscriptionForm()

    if no_inscrits_confAM > max_confAM-1:
        form.fields['confAM'].widget.attrs['disabled'] = True
        msg_confAM = "--- COMPLET ---"
    if no_inscrits_confPM > max_confPM-1:
        form.fields['confPM'].widget.attrs['disabled'] = True
        msg_confPM = "--- COMPLET ---"
    if no_inscrits_workshop_l1 > max_wshopL1-1:
        form.fields['workshop_L1'].widget.attrs['disabled'] = True
        msg_wshopL1 = "--- COMPLET ---"
    if no_inscrits_workshop_l2 > max_wshopL2-1:
        form.fields['workshop_L2'].widget.attrs['disabled'] = True
        msg_wshopL2 = "--- COMPLET ---"

    return render(request, 'main/inscriptions.html', locals())

def organisateurs(request):
    users = User.objects.order_by('first_name')
    mid_number_users = len(users) / 2
    return render(request, 'main/organisateurs.html', locals())

def partenaires(request):
    return render(request, 'main/partenaires.html')

def telechargements(request):
    return render(request, 'main/telechargements.html')

def localisation(request):
    return render(request, 'main/localisation.html')

