$( document ).ready(function() {
    /*-------------   Gestion des checkbox pour le choix des évenements -----------------*/

    $("input[type='checkbox']").click(function() {
    /*-- Gestion du choix des workshop --*/

        if($('input#id_workshop_L2').is(":checked")){
            $('input#id_workshop_L1').prop('disabled', true);
        }else{
            $('input#id_workshop_L2').prop('disabled', true);
        }
        if($('input#id_workshop_L1').is(":checked")==false && $('input#id_workshop_L2').is(":checked")==false){
            $('input#id_workshop_L2').prop('disabled', false);
            $('input#id_workshop_L1').prop('disabled', false);
        }

        if($('input#id_workshop_L1').is(":checked") && $('input#id_workshop_L2').is(":checked"))
        {
            $(this).prop('checked', false);
        }
    });
});
